//
//  Deck.m
//  Matchismo-Assignment1
//
//  Created by Ashley Ng on 11/1/14.
//  Copyright (c) 2014 Ashley Ng. All rights reserved.
//

/// PRIVATE API ///
#import "Deck.h"

@interface Deck()

/// store cards in deck
@property (strong, nonatomic) NSMutableArray *cards;

@end

@implementation Deck

/// cards getter
- (NSMutableArray *)cards {
    
    /// if _cards not initialized/allocated yet, do so
    if (!_cards) {
        _cards = [[NSMutableArray alloc] init];
    }
    return _cards;
}

- (void)addCard:(Card *)card atTop:(BOOL)atTop {
    if (atTop) {
        [self.cards insertObject:card atIndex:0];   /// add to front of array
    }
    else {
        [self.cards addObject:card];    /// add to end of array
    }
}

- (void)addCard:(Card *)card {
    [self addCard:card atTop:NO];
}

- (Card *)drawRandomCard {
    
    Card *randomCard = nil;
    
    if ([self.cards count]) {
        /// get random card, store it, then delete it from array
        unsigned index = arc4random() % [self.cards count];
        randomCard = self.cards[index];
        [self.cards removeObjectAtIndex:index];
    }
    
    return randomCard;
}

- (NSUInteger)size {
    return [self.cards count];
}

@end
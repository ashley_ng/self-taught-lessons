//
//  PlayingCard.h
//  Matchismo-Assignment1
//
//  Created by Ashley Ng on 11/1/14.
//  Copyright (c) 2014 Ashley Ng. All rights reserved.
//

/// PUBLIC API ///
#import <Foundation/Foundation.h>
#import "Card.h"

#ifndef Matchismo_Assignment1_PlayingCard_h
#define Matchismo_Assignment1_PlayingCard_h

@interface PlayingCard : Card

@property (strong, nonatomic) NSString *suit;
@property (nonatomic) NSUInteger rank;

+ (NSArray *)validSuits;
+ (NSUInteger)maxRank;
@end

#endif

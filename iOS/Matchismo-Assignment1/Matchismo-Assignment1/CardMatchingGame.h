//
//  CardMatchingGame.h
//  Matchismo-Assignment1
//
//  Created by Ashley Ng on 11/2/14.
//  Copyright (c) 2014 Ashley Ng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Deck.h"
#import "Card.h"

#ifndef Matchismo_Assignment1_CardMatchingGame_h
#define Matchismo_Assignment1_CardMatchingGame_h

@interface CardMatchingGame : NSObject

/// designated initializer
- (instancetype)initWithCardCount: (NSUInteger)count
                        usingDeck: (Deck *)deck;

- (void)chooseCardAtIndex: (NSUInteger)index;
- (Card *)cardAtIndex: (NSUInteger)index;

@property (nonatomic, readonly) NSInteger score;
@end


#endif

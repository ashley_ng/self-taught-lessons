//
//  PlayingCardDeck.h
//  Matchismo-Assignment1
//
//  Created by Ashley Ng on 11/1/14.
//  Copyright (c) 2014 Ashley Ng. All rights reserved.
//

/// PUBLIC API ///
#import <Foundation/Foundation.h>
#import "Deck.h"

#ifndef Matchismo_Assignment1_PlayingCardDeck_h
#define Matchismo_Assignment1_PlayingCardDeck_h

@interface PlayingCardDeck : Deck

@end


#endif

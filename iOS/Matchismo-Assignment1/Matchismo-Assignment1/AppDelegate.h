//
//  AppDelegate.h
//  Matchismo-Assignment1
//
//  Created by Ashley Ng on 11/1/14.
//  Copyright (c) 2014 Ashley Ng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


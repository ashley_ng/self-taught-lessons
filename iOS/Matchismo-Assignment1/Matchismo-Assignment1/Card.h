//
//  Card.h
//  Matchismo-Assignment1
//
//  Created by Ashley Ng on 11/1/14.
//  Copyright (c) 2014 Ashley Ng. All rights reserved.
//

/// PUBLI API ///
#import <Foundation/Foundation.h>

#ifndef Matchismo_Assignment1_Card_h
#define Matchismo_Assignment1_Card_h

@interface Card : NSObject

@property (strong, nonatomic) NSString *contents;

@property (nonatomic, getter=isChosen) BOOL chosen;
@property (nonatomic, getter=isMatched) BOOL matched;

- (int)match: (NSArray *)otherCards;

@end


#endif

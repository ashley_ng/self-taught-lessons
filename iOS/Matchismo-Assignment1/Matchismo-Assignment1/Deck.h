//
//  Deck.h
//  Matchismo-Assignment1
//
//  Created by Ashley Ng on 11/1/14.
//  Copyright (c) 2014 Ashley Ng. All rights reserved.
//


/// PUBLIC API ///
#import <Foundation/Foundation.h>
#import "Card.h"

#ifndef Matchismo_Assignment1_Deck_h
#define Matchismo_Assignment1_Deck_h

@interface Deck : NSObject

- (void)addCard: (Card *)card atTop:(BOOL)atTop;
- (void)addCard:(Card *)card;

- (Card *)drawRandomCard;
- (NSUInteger) size;

@end

#endif

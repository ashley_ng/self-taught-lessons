//
//  PlayingCardDeck.m
//  Matchismo-Assignment1
//
//  Created by Ashley Ng on 11/1/14.
//  Copyright (c) 2014 Ashley Ng. All rights reserved.
//

/// PRIVATE API ///
#import "PlayingCardDeck.h"
#import "PlayingCard.h"

@implementation PlayingCardDeck

/// initialize a playing card deck
- (instancetype)init {
    self = [super init];
    
    if (self) {
        /// go through all the types of suits and ranks and create the card
        /// then add to the deck
        for (NSString *suit in [PlayingCard validSuits]) {
            for (NSUInteger rank = 1; rank <= [PlayingCard maxRank]; rank++) {
                PlayingCard *card = [[PlayingCard alloc] init];
                card.rank = rank;
                card.suit = suit;
                [self addCard:card];
            }
        }
    }
    
    return self;
}

@end

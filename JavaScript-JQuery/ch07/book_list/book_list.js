$(document).ready(function()
	{
		$("h2").toggle(
			function() {
				$(this).addClass("minus");
				$(this).next().show();
			},
			function() {
				$(this).removeClass("minus");
				$(this).next().hide();
				$("aside img").attr("src", "");
			}
		);
		
		//preload images
		$("a").each(
			function() {
				var image = new Image();
				image.src = $(this).attr("href");	
			}
		);
		
		$("a").click(
			function(evt) {
				var imageURL = $(this).attr("href");
				$("aside img").attr("src", imageURL);
				
				//cancel default action
				evt.preventDefault();
			}
		);
	}
);

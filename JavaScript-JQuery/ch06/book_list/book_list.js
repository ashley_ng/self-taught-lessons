var $ = function (id) {
	return document.getElementById(id);
};

window.onload = function () {
    var cats = $("categories");
    var h2_elements = cats.getElementsByTagName("h2");

    		    
    var headingNode;
    for (var i = 0; i < h2_elements.length; i++ ) {
    	headingNode = h2_elements[i];
    	
    	// Attach event handler
    	headingNode.onclick = function () {
			var h2 = this;         // h2 is the current headingNode object
			if (h2.getAttribute("class") == "plus") {
				h2.setAttribute("class", "minus");	
			}
			else {
				h2.setAttribute("class", "plus");
			}
			if (h2.nextElementSibling.getAttribute("class") == "closed") {
				h2.nextElementSibling.setAttribute("class", "open");
			}
			else {
				h2.nextElementSibling.setAttribute("class", "closed");
				$("image").setAttribute("src", "");
			} 
		};
    }
    
    var listNode = $("categories");
    var imageNode = $("image");
    
    var imageLinks = listNode.getElementsByTagName("a");
    
    var i, linkNode, image;
    for(i = 0; i < imageLinks.length; i++) {
    	linkNode = imageLinks[i];
    	
    	linkNode.onclick  = function(evt)	{
    		var link = this;
    		imageNode.src = link.getAttribute("href");
    		//imageNode.src = link.setAttribute("style", "display: block;");
    		
    		// Cancel the default action of the event
	    	if (!evt) { evt = window.event; }
		    if ( evt.preventDefault ) {
		        evt.preventDefault();          // DOM compliant code
		    }
		    else {
		        evt.returnValue = false;
		    }
    	};
    	image = new Image();
    	image.src = linkNode.getAttribute("href");
    }
};

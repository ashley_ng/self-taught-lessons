var $ = function (id) {
    return document.getElementById(id);
}

var calculateClick = function() {
    var investment = parseFloat($("investment").value);
    var rate = parseFloat($("rate").value);
    var years = parseInt($("years").value);
    
    if (isNaN(investment) || investment <= 0) {
        alert("Investment Amount must be a number greater than 0");
    }
    else if (isNaN(rate) || rate <= 0) {
        alert("Annual Interest Rate must be a number greater than 0");
    }
    else if (isNaN(rate) || years <= 0) {
        alert("Years must be a number greater than 0");
    }
    else {
        for (x = 0; x < years; x++) {
         investment = investment + (investment * (rate/100));
        }
        $("future_value").value = investment.toFixed(0);
    }
}

window.onload = function() {
    $("calculate").onclick = calculateClick;
    $("investment").focus();
}
Self-Taught Lesson/Languages
===========================
Taught myself LaTeX (from online resources), HTML and CSS, and JavaScript and JQuery from Murach Training books

### LaTeX Resources
* [Reddit LaTeX Lessons](http://latex101.wikispaces.com/)
* Washington & Jefferson College [Lessons](http://www2.washjeff.edu/users/rhigginbottom/latex/main.html) (Math 233 by Ryan Higgibottom)

### Murach books
* [Murach's HTML5 and CSS3](https://murach.com/books/htm5/index.htm)
* [Murach's JavaScript and JQuery](https://murach.com/books/qury/index.htm)

### Objective-C
* [Big Nerd Ranch (2nd Edition)](http://www.bignerdranch.com/we-write/objective-c-programming.html)

### iOS
* [Stanford](https://itunes.apple.com/us/course/developing-ios-7-apps-for/id733644550) CS193p, Fall 2013/14, Paul Hegarty
* Ray Wenderlich's swift iOS [tutorials](http://www.raywenderlich.com/tutorials)

//
//  main.m
//  Challenges
//
//  Created by Ashley Ng on 9/23/14.
//  Copyright (c) 2014 Ashley Ng. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <math.h>
#import <readline/readline.h>
#import <stdio.h>

void challenge1();
void challenge2();
void challenge5();
float remaingingAngle(float angleA, float angleB);
void challenge6(int number);
void challenge7();
void challenge8();
void challenge8_2();
void challenge9();


int main(int argc, const char * argv[]) {
    //    @autoreleasepool {
    //        // insert code here...
    //        NSLog(@"Hello, World!");
    //    }
    
    challenge8_2();
    
    return 0;
}

void challenge1(){
    float one = 3.14;
    float two = 42.0;
    double sum = one + two;
    printf("%f\n", sum);
}

void challenge2(){
    int i = 20;
    int j = 25;
    int k = (i > j) ? 10 : 5;
    
    if (5 < j - k) {
        printf("The first expression is true");
    }
    else if (j > i) {
        printf("the second expression is true");
    }
    else {
        printf("Neither expression is true");
    }
}

void challenge5() {
    float angleA = 30.0;
    float angleB = 60.0;
    float angleC = remaingingAngle(angleA, angleB);
    printf("The third angle is %.2f\n", angleC);
}
float remaingingAngle(float angleA, float angleB){
    return 180 - (angleA + angleB);
}

void challenge6(int number) {
    printf("\"%d\" squared is \"%d\"\n", number, (number*number));
}

void challenge7(){
    double x = 1;
    double temp = sin(x);
    printf("%.3f\n", temp);
    
}

void challenge8(){
    printf("Who is cool? ");
    const char *name = readline(NULL);
    printf("%s is cool!\n\n", name);
}
void challenge8_2(){
    printf("Where should I start counting? ");
    const char *numChar = readline(NULL);
    int num = atoi(numChar);
    for (int x = num; x >= 0; x--) {
        printf("%d\n", x);
    }
}